console.log(document);

const txtFirstName = document.querySelector("#txt-first-name"); // flexible - id or class
// result: input filed / tag
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

/* 
    Alternative ways 
    > document.getElementById("id-name");
    > document.getElementByClassName("text-class");
    > document.getElementByTagName("h1");
*/

// target the full name
// Event Listerners
// event can have a shorthand of (e)
// every tag has innerHTML = dynamic value

txtFirstName.addEventListener('keyup', (event) => {
    // spanFullName.innerHTML = txtFirstName.value;
    //console.log(event);
    console.log(event.target); // .target indicates or targets input field
})

const keyCodeEvent = (e) => {
    let kc = e.keyCode;
    if (kc === 65) {
        e.target.value = null;
        alert("Someone clicked a");
    }
}

txtFirstName.addEventListener('keyup', keyCodeEvent);

// Activity Sol'n

const firstnameCapture = (e) => {
    spanFullName.innerHTML = txtFirstName.value;
}

const lastnameCapture = (e) => {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener('keyup', firstnameCapture);
txtLastName.addEventListener('keyup', lastnameCapture);
